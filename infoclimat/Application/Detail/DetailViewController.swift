//
//  DetailViewController.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit

final class DetailViewController: UITableViewController, StoryboardBased {
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var temperature2mLabel: UILabel!
    @IBOutlet private weak var ventMoyenLabel: UILabel!
    @IBOutlet private weak var ventRafalesLabel: UILabel!
    
    @IBOutlet private weak var zeroDegreLabel: UILabel!
    @IBOutlet private weak var risqueDeNeigeLabel: UILabel!
    @IBOutlet private weak var humiditeLabel: UILabel!
    @IBOutlet private weak var pressionLabel: UILabel!
    
    var gfsDetailVM: GfsDetailViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    func setup() {
        self.title = self.gfsDetailVM.date ?? ""
        self.temperatureLabel.text = self.gfsDetailVM.temperatureSol
        self.temperature2mLabel.text = self.gfsDetailVM.temperature2m
        self.ventMoyenLabel.text = self.gfsDetailVM.ventMoyen
        self.ventRafalesLabel.text = self.gfsDetailVM.ventRafales
        
        self.zeroDegreLabel.text = self.gfsDetailVM.zeroDegre
        self.risqueDeNeigeLabel.text = self.gfsDetailVM.risqueDeNeige
        self.humiditeLabel.text = self.gfsDetailVM.humidite
        self.pressionLabel.text = self.gfsDetailVM.pression
    }
    
}
