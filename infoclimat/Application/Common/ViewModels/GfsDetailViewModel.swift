//
//  GfsDetail.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import SwiftyJSON

class GfsDetailViewModel {
    let date: String?
    var temperatureSol: String = "-"
    var temperature2m: String = "-"
    var ventMoyen: String = "-"
    var ventRafales: String = "-"
    var zeroDegre: String = "-"
    var risqueDeNeige: String = "-"
    var humidite: String = "-"
    var pression: String = "-"
    
    init(date: String, json: JSON) {
        self.date = date
        if let temperature = json["temperature"]["sol"].int {
            self.temperatureSol = "\(Float(temperature) - 273.15)°C"
        }
        if let temperature = json["temperature"]["2m"].int {
            self.temperature2m = "\(Float(temperature) - 273.15)°C"
        }
        if let vent = json["vent_moyen"]["10m"].float {
            self.ventMoyen = "\(vent) km/h"
        }
        if let vent = json["vent_rafales"]["10m"].float {
            self.ventRafales = "\(vent) km/h"
        }
        if let zeroDegre = json["iso_zero"].int {
            self.zeroDegre = "\(zeroDegre) m"
        }
        if let risqueDeNeige = json["risque_neige"].string {
            self.risqueDeNeige = "\(risqueDeNeige)"
        }
        if let pression = json["pression"]["niveau_de_la_mer"].int {
            self.pression = "\(pression/100) hPA"
        }
        if let humidite = json["humidite"]["2m"].float {
            self.humidite = "\(humidite)%"
        }
    }
}
