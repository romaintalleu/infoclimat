//
//  DateFormatter.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation

extension DateFormatter {
    static private let webServiceFullFormat = "yyyy-MM-dd HH:mm:ss"
    static private let webServiceDayFormat = "d MMMM HH"
    
    static let webServiceFullFormatter = DateFormatter(format: DateFormatter.webServiceFullFormat)
    static let webServiceDayFormatter = DateFormatter(format: DateFormatter.webServiceDayFormat)
    
    private convenience init(format: String) {
        self.init()
        dateFormat = format
    }
}
