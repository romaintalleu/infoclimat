//
//  ICWebServiceClientTests.swift
//  infoclimatTests
//
//  Created by Romain Talleu on 14/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import XCTest
import RxSwift

// MARK: Constants

private let kTimeout: TimeInterval = 15.0

// MARK: - Test Case

class ICWebServiceClientTests: XCTestCase {
    
    private let disposeBag = DisposeBag()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchGfs() {
        let expectation = self.expectation(description: "WebService fetchGfs fulfill")
        let webService = ICWebServiceClient()
        
        webService.fetchGfs()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { gfs in
                XCTAssertFalse(gfs.isEmpty)
            }, onError: { error in
                XCTAssertNil(error, "error : \(error)")
            }, onCompleted: nil, onDisposed: {
                expectation.fulfill()
            })
            .addDisposableTo(self.disposeBag)
        
        self.waitForExpectations(timeout: kTimeout) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
        }
    }
}
