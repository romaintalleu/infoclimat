//
//  ICRoute.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import Alamofire

public enum Route: URLConvertible {
    case gfs
    
    var path: String {
        switch self {
        case .gfs: return "/public-api/gfs/json"
        }
    }
    
    // MARK: URLConvertible
    public func asURL() throws -> URL {
        return URL(string: "http://www.infoclimat.fr" + path)!
    }
}
