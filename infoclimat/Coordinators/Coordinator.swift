//
//  Coordinator.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit

// MARK: Protocol

protocol Coordinator: class {
    var mainViewController: UIViewController { get }
    var childCoordinators: [Coordinator] { get set }
    func pushChildCoordinator(coordinator: Coordinator)
    func popChildCoordinator()
}

//MARK: Default Implementation
extension Coordinator {
    func pushChildCoordinator(coordinator: Coordinator) {
        self.childCoordinators.append(coordinator)
        self.mainViewController.present(coordinator.mainViewController, animated: true) { }
    }
    
    func popChildCoordinator() {
        _ = self.childCoordinators.popLast()
        self.mainViewController.dismiss(animated: true) { }
    }
}

protocol NavCoordinator: Coordinator {
    var navigationController: UINavigationController { get }
}

extension NavCoordinator {
    var navigationController: UINavigationController {
        guard let nc = self.mainViewController as? UINavigationController else {
            fatalError("The rootViewController should be a UINavigationController")
        }
        return nc
    }
    
    func pushRootViewController(viewController: UIViewController) {
        self.navigationController.setViewControllers([viewController], animated: false)
    }
    
    func pushOfflineRootViewController(viewController: UIViewController) {
        self.navigationController.setViewControllers([viewController], animated: false)
    }
    
    func pushController(viewController: UIViewController) {
        self.navigationController.pushViewController(viewController, animated: true)
    }
}
