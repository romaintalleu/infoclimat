//
//  NSObject.swift
//  Huge
//
//  Created by Romain Talleu on 12/07/2017.
//  Copyright © 2017 rtalleu. All rights reserved.
//

import Foundation

extension NSObject {
    static func className() -> String {
        return String(describing: self)
    }
}
