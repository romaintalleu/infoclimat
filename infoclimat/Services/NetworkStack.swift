//
//  NetworkStack.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RxSwift

class NetworkStack {
    
    enum ErrorType: Error {
        // No internet, roaming off, data not allowed, call active, …
        case noInternet(error: Error)
        // DNS Lookup failed, Host unreachable, …
        case serverUnreachable(error: Error)
        // Invalid request, Fail to parse JSON, Unable to decode payload…
        case badServerResponse(error: Error)
        // Response in 4xx-5xx range
        case HTTP(statusCode: Int, data: Data?)
        // Fail to parse response
        case ParseError
        // Other, unclassified error
        case otherError(error: Error)
        // Unknown
        case unknow
    }
    
    // MARK: Properties
    
    private let requestManager: SessionManager
    
    // MARK: Setup
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.urlCache = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        self.requestManager = SessionManager(configuration: configuration)
    }
    
    // MARK: Public methods
    
    func sendJSONRequest(url: URLConvertible, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> Observable<JSON> {
        let request = self.requestManager.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
        return self.sendRequest(alamofireRequest: request)
    }
    
    // MARK: Private methods
    
    private func sendRequest(alamofireRequest: DataRequest) -> Observable<JSON> {
        return Observable.create { observer in
            self.validateRequest(request: alamofireRequest).responseJSON { response in
                if let value = response.result.value {
                    observer.onNext(JSON(value))
                    observer.onCompleted()
                } else {
                    let stackError = self.networkStackError(error: response.error!, httpURLResponse: response.response, responseData: response.data)
                    observer.onError(stackError)
                    observer.onCompleted()
                }
                
            }
            
            return Disposables.create {
                alamofireRequest.cancel()
            }
        }
    }
    
    private func networkStackError(error: Error, httpURLResponse: HTTPURLResponse?, responseData: Data? = nil) -> ErrorType {
        let finalError: ErrorType
        switch error {
        case URLError.notConnectedToInternet, URLError.cannotLoadFromNetwork, URLError.networkConnectionLost, URLError.callIsActive, URLError.internationalRoamingOff, URLError.dataNotAllowed, URLError.timedOut:
            finalError = ErrorType.noInternet(error: error)
        case URLError.cannotConnectToHost, URLError.cannotFindHost, URLError.dnsLookupFailed, URLError.redirectToNonExistentLocation:
            finalError = ErrorType.serverUnreachable(error: error)
        case URLError.badServerResponse, URLError.cannotParseResponse, URLError.cannotDecodeContentData, URLError.cannotDecodeRawData:
            finalError = ErrorType.badServerResponse(error: error)
        default:
            let statusCode = httpURLResponse?.statusCode ?? 0
            finalError = 400..<600 ~= statusCode ? ErrorType.HTTP(statusCode: statusCode, data: responseData) : ErrorType.otherError(error: error)
        }
        
        return finalError
    }
    
    private func validateRequest(request: DataRequest) -> DataRequest {
        return request.validate(statusCode: 200 ..< 300)
    }
}
