//
//  ICParameter.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import Alamofire

enum Parameter {
    case gfs
    
    var value: Parameters {
        switch self {
        case .gfs: return ["_ll": "48.85341,2.3488",
                           "_auth": "ABpQRwJ8U3FVeAQzBXNReFU9BzIAdldwVysGZQ5rAH0IYwBhVDRWMFU7Uy4OIQQyU34FZgw3AjJROlYuDX9SMwBqUDwCaVM0VToEYQUqUXpVewdmACBXcFc1BmEOagB9CGkAYVQpVjVVO1M2DiAEMVNhBXoMLAI7UTdWMQ1nUjgAYlA2AmlTOFU9BHkFKlFgVW8HMgA/VzpXZwYyDmIAYQg5AG1UPlZlVT1TLw45BDRTYwVlDDMCPlE0VjMNf1IuABpQRwJ8U3FVeAQzBXNReFUzBzkAaw==",
                           "_c": "0156460ec01747784c548bca70e8ab92"]
        }
    }
}
