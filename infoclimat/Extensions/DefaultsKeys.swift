//
//  DefaultsKeys.swift
//  infoclimat
//
//  Created by Romain Talleu on 14/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import SwiftyUserDefaults

extension DefaultsKeys {
    static let gfsJSON = DefaultsKey<String?>("gfs_json")
}
