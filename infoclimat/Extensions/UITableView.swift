//
//  UITableView.swift
//  Huge
//
//  Created by Romain Talleu on 12/07/2017.
//  Copyright © 2017 rtalleu. All rights reserved.
//

import Foundation
import UIKit

public extension UITableView {
    
    func registerCellClass(cellClass: AnyClass) {
        let identifier = String(describing: cellClass)
        self.register(cellClass, forCellReuseIdentifier: identifier)
    }
    
    func registerCellNib(cellClass: AnyClass) {
        let identifier = String(describing: cellClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forCellReuseIdentifier: identifier)
    }
    
    func registerHeaderFooterNib(cellClass: AnyClass) {
        let identifier = String(describing: cellClass)
        let nib = UINib(nibName: identifier, bundle: nil)
        self.register(nib, forHeaderFooterViewReuseIdentifier: identifier)
    }
    
    func registerHeaderFooterClass(cellClass: AnyClass) {
        let identifier = String(describing: cellClass)
        self.register(cellClass, forHeaderFooterViewReuseIdentifier: identifier)
    }
    
}

@IBDesignable class CustomTableView: UITableView {
    @IBInspectable var backgroundImage: UIImage?
    
    
    // MARK: UIView
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // set backgroundImage
        let backgroundContenairView = UIImageView(frame: self.frame)
        backgroundContenairView.contentMode = .scaleAspectFill
        backgroundContenairView.image = self.backgroundImage
        
        self.backgroundView = backgroundContenairView
    }
}
