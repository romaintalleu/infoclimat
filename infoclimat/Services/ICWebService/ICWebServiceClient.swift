//
//  ICWebServiceClient.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RxSwift

class ICWebServiceClient {
    private let networkStack: NetworkStack
    
    init() {
        self.networkStack = NetworkStack()
    }
    
    // MARK: - Public methods
    
    // MARK: - GFS
    
    func fetchGfs() -> Observable<JSON> {
        return self.sendJSONRequest(url: Route.gfs, method: .get, parameters: Parameter.gfs.value)
    }
    
    // MARK: - Private methods
    
    private func sendJSONRequest(url: URLConvertible, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> Observable<JSON> {
        return self.networkStack.sendJSONRequest(url: url, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }
}
