//
//  GfsViewModel.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation
import SwiftyJSON

class GfsViewModel {
    var gfs = [GfsDetailViewModel]()
    
    init(json: JSON) {
        
        //Need sort because wrong order of keys in json.
        let jsonSorted = json.sorted(by: { (l: (String, JSON), r: (String, JSON)) -> Bool in
            let dateFormatter = DateFormatter.webServiceFullFormatter
            if let lDate = dateFormatter.date(from: l.0), let rDate = dateFormatter.date(from: r.0) {
                return lDate.isLessThanDate(dateToCompare: rDate)
            }
            return true
        })
        
        //Create detail ViewModel
        jsonSorted.forEach { (json: (String, JSON)) in
            self.gfs.append(GfsDetailViewModel(date: json.0, json: json.1))
        }
        
        //Filter to keep keys we need
        self.gfs = self.gfs.filter { gfs -> Bool in
            //TODO: Change 2017 to current year.
            return gfs.date?.contains("2017") ?? false
        }
    }
}
