//
//  String.swift
//  infoclimat
//
//  Created by Romain Talleu on 15/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
