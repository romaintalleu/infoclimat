//
//  ICAppCoordinator.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyUserDefaults
import SwiftyJSON

class ICAppCoordinator: NSObject, Coordinator {
    
    // MARK: properties
    var mainViewController: UIViewController = UIViewController()
    var window: UIWindow!
    
    var childCoordinators: [Coordinator] = [Coordinator]()
    var webService = ICWebServiceClient()
    
    private let disposeBag = DisposeBag()
    
    fileprivate lazy var homeCoordinator: HomeCoordinator = HomeCoordinator(coordinator: self, webService: self.webService, appCoordinator: self)
    
    // MARK: Public methods
    
    func start(window: UIWindow) {
        self.window = window
        window.makeKeyAndVisible()
        
        self.startLauchApplication()
    }
    
    // MARK: Private methods
    
    private func startLauchApplication() {
        self.initApplicationContext()
        
        let splashscreenVC = SplashscreenViewController.instanciate()
        self.window.rootViewController = splashscreenVC
        
        self.webService.fetchGfs()
            .delay(1.5, scheduler: MainScheduler.instance)
            .subscribe(onNext: { gfs in
                let gfsVM = GfsViewModel(json: gfs)
                Defaults[.gfsJSON] = gfs.rawString()
                self.startApplication(gfsVM: gfsVM)
            }, onError: { error in
                if let gfsString = Defaults[.gfsJSON] {
                    let gfsJSON = JSON(parseJSON: gfsString)
                    let gfsVM = GfsViewModel(json: gfsJSON)
                    self.startApplication(gfsVM: gfsVM)
                }
            }, onCompleted: nil, onDisposed: nil)
            .addDisposableTo(self.disposeBag)
    }
    
    private func startApplication(gfsVM: GfsViewModel) {
        self.homeCoordinator.showController(gfsVM: gfsVM)
        self.window.rootViewController = self.homeCoordinator.mainViewController
    }
    
    private func initApplicationContext() {
        //TODO: User Fabric
        //Fabric.with([Crashlytics.self])
    }
}

// MARK: coordinator delegates
extension ICAppCoordinator: HomeCoordinatorDelegate {
    
}
