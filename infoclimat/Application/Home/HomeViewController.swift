//
//  HomeViewController.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit

private struct SizeClassMetric<T> {
    let compact: T
    let regular: T
    func currentValue(env: UITraitEnvironment) -> T {
        return (env.traitCollection.horizontalSizeClass == .compact) ? compact : regular
    }
}

final class HomeViewController: UIViewController, StoryboardBased {
    @IBOutlet private weak var tableView: UITableView!
    
    fileprivate let heightRow = SizeClassMetric<CGFloat>(compact: 60.0, regular: 100.0)
    
    var gfsVM: GfsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.setupTableView()
    }
    
    private func setupUI() {
        self.title = "home_title_navbar".localized
    }
    
    private func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.registerCellNib(cellClass: DayTableViewCell.self)
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gfsVM.gfs.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.heightRow.currentValue(env: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DayTableViewCell.className(), for: indexPath) as! DayTableViewCell
        cell.setup(gfsDetail: self.gfsVM.gfs[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let gfsDetailVM = self.gfsVM.gfs[indexPath.row]
        let detailVC = DetailViewController.instanciate()
        detailVC.gfsDetailVM = gfsDetailVM
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}
