//
//  HomeCoordinator.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit

// MARK: delegates

protocol HomeCoordinatorDelegate: class {
    
}

class HomeCoordinator: NSObject, Coordinator, NavCoordinator {
    
    // MARK: properties
    var mainViewController = UIViewController()
    var childCoordinators = [Coordinator]()
    
    private weak var coordinatorDelegate: HomeCoordinatorDelegate?
    private weak var appCoordinator: ICAppCoordinator?
    private var webService: ICWebServiceClient!
    
    // MARK: init coordinator
    init(coordinator delegate: HomeCoordinatorDelegate, webService: ICWebServiceClient, appCoordinator: ICAppCoordinator) {
        super.init()
        self.coordinatorDelegate = delegate
        self.webService = webService
        self.appCoordinator = appCoordinator
        
        self.mainViewController = UINavigationController(rootViewController: UIViewController())
    }
    
    // MARK: Public methods
    func popToRootViewController(animated: Bool = true) {
        self.navigationController.popViewController(animated: animated)
    }
    
    func showController(gfsVM: GfsViewModel) {
        let homeVC = HomeViewController.instanciate()
        homeVC.gfsVM = gfsVM
        self.pushRootViewController(viewController: homeVC)
    }
}
