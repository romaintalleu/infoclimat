//
//  Splashscreen.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit

final class SplashscreenViewController: UIViewController, StoryboardBased {
    
    // MARK: life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
