//
//  DayTableViewCell.swift
//  infoclimat
//
//  Created by Romain Talleu on 13/10/2017.
//  Copyright © 2017 Romain Talleu. All rights reserved.
//

import UIKit

class DayTableViewCell: UITableViewCell {
    @IBOutlet private weak var dayLabel: UILabel!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var humiditeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(gfsDetail: GfsDetailViewModel) {
        let dateFormatter = DateFormatter.webServiceFullFormatter
        if let dateString = gfsDetail.date, let date = dateFormatter.date(from: dateString) {
            let webServiceDayFormatter = DateFormatter.webServiceDayFormatter
            self.dayLabel.text = "\(webServiceDayFormatter.string(from: date))h"
        }
        self.temperatureLabel.text = gfsDetail.temperatureSol
        self.humiditeLabel.text = gfsDetail.humidite
    }
}
